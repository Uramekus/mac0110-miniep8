#Variavel global estatica e imutavel contendo o primeiro digito possivel de cada carta, ja que o primeiro valor é ASCII e cabe em um byte podemos usar o index facilmente
const a = ['2', '3', '4', '5', '6', '7', '8', '9', '1', 'J', 'Q', 'K', 'A']

function troca(v, i, j)
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end

function insercao(v)
	tam = length(v)
	for i in 2:tam
		j = i
		while j > 1
			if compareByValueAndSuit(v[j], v[j - 1])
				troca(v, j, j - 1)
			else
				break
			end
			j = j - 1
		end
	end
	return v
end

function compareByValue(x,y) 
    🐸 = findfirst(isequal(x[1]), a)
    🐵 = findfirst(isequal(y[1]), a)
    #nao pode ser oneliner por causa de ::Nothing handling de Julia
    #println(x[1],": ", findfirst(isequal(x[1]), a),"  ", y[1],": ", findfirst(isequal(y[1]), a))
    return !isnothing(🐸) && !isnothing(🐵) ? 🐸 < 🐵 ?  true :  false : error("um ou mais dos valores nao existe/error")
end

function compareByValueAndSuit(x, y)
    n = ['♦', '♠', '♥', '♣']
    #primeiro comparamos os naipes, se os naipes forem iguais, comparamos os valores
    🦇 = findfirst(isequal(x[end]), n)
    🐀 = findfirst(isequal(y[end]), n)

    🦇 != 🐀 ? nothing : return(compareByValue(x,y))

    return !isnothing(🦇) && !isnothing(🐀) ? 🦇 < 🐀 ?  true :  false : error("um ou mais dos valores nao existe/error")
end

using Test
function test()
    @test compareByValue("2♠", "A♠")
    @test !compareByValue("K♥", "10♥")
    @test !compareByValue("10♠", "10♥")
    @test !compareByValue("10♡","2♡")

    @test compareByValueAndSuit("2♠", "A♠")
    @test !compareByValueAndSuit("K♥", "10♥")
    @test compareByValueAndSuit("10♠", "10♥")
    @test compareByValueAndSuit("A♠", "2♥")

    println("testes feitos com sucesso")
end

test()
